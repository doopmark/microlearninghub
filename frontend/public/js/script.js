
// ======= eto ay para sa navbar pag nag scroll mag aadd lng ng black background
if(window.innerWidth >= 1024) { //etong if chinecheck lng kung > = ba sa 1024 na screen if true transparent ung nav
  let scrollpos = window.scrollY
  const header = document.querySelector("nav")
  const add_class_on_scroll = () => header.classList.add("fade-in")
  const remove_class_on_scroll = () => header.classList.remove("fade-in")
  console.log(window.screen.width)
  window.addEventListener('scroll', function() { 
      scrollpos = window.scrollY;
      if (scrollpos >= 300) { add_class_on_scroll() }
      else { remove_class_on_scroll() }
      // console.log(scrollpos)
  })
} else { // else na mababa sa 1024 ung screen mag aadd lng dito ng black background sa nav
  const header = document.querySelector("nav")
  header.classList.add("fade-in");
}


let navbar = document.querySelector('.navbar-nav'); 
if(navbar) { //if true na ung navbar ay nasa current loaded page
  if(localStorage.length == 0) { //chineck ko lng ung localStorage (localStorage dito tayo nag sesave ng token at user infos if na may laman to may nkalogin)
    //if walang laman si local storage which is length nya ay == 0 eto ang nav links dapat na mag rerender
    navbar.innerHTML = ` 
    <li class="nav-item">
      <a class="nav-link" href="#home">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link classScroll" href="#classes">Classes</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#testimonial">Testimonials</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" data-toggle="modal" data-target="#loginModal">Login</a>
    </li>
    <li class="nav-item">
      <a class="nav-link registerLink" href="#" data-toggle="modal" data-target="#registerModal">Register</a>
    </li>
    `
  } else {
    // if merong laman ung localStorage means may nka log in check lng kung admin ba to or user
    if(localStorage.getItem('isAdmin') == 'true') {
      // admin nka log in eto ang nav links na mag rerender
      navbar.innerHTML = `
      <li class="nav-item">
      <a class="nav-link" href="#home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link classScroll" href="#classes">Classes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#testimonial">Testimonials</a>
      </li>
      <li class="nav-item ">
      <a class="nav-link" href="#" id="allUsers" data-toggle="modal" data-target="#usersModal">Users</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#addClassesModal">Add Classes</a> 
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown">
            <img class="avatarNav" src="http://localhost:5000/avatar/${localStorage.getItem('avatar')}" alt="" />
            Admin
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownId">
              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#profileModal">Profile</a>
              <a class="dropdown-item logout" href="#">Logout</a>
          </div>
      </li>
      `
    } else { 
      //else user ang nka log in so mga nav links dapat pang user lang
      navbar.innerHTML = `
      <li class="nav-item activeNav">
      <a class="nav-link" href="#home">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link classScroll" href="#classes">Classes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#testimonial">Testimonials</a>
      </li>
      <li class="nav-item dropdown">
          
          <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown">
            <img class="avatarNav" src="http://localhost:5000/avatar/${localStorage.getItem('avatar')}" alt="" />
            ${localStorage.firstname}
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownId">
              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#profileModal">Profile</a>
              <a class="dropdown-item logout" href="#">Logout</a>
          </div>
      </li>
      `
    }
  }
}
if (localStorage.length == 0) { //same  thing pero iba lng ng function
  //pag walang nka log in imbis na dalhin tayo sa classes nung BOOK NOW btn sa landing page ilalabas nya dapat ung register
  document.querySelector('.custom-btn').addEventListener('click', function() {
    $('#registerModal').modal('toggle');
  });
} else {
  //if may user na nka log in mag iiscroll lng tayo sa classes 
  document.querySelector('.custom-btn').addEventListener('click', function() {
    document.querySelector('.classScroll').click();
  })
}


let btnReg = document.querySelector('.btnRegSubmit');
let btnLog = document.querySelector('.loginSubmit');

if(btnReg) { //same thing chineck ko lng kung ung btn na to ay nsa current loaded page
  const registerClicked = function() { // etong function na to ttwagin ko lng to sa addEventlistner na imbis na gngawa natin na addEventListner('click', function() {})
  // same thing lang nmn 
    let firstname = document.querySelector('#firstname').value; 
    let lastname = document.querySelector('#lastname').value;
    let email = document.querySelector('#email').value;
    let password = document.querySelector('#password').value;
    let password2 = document.querySelector('#password2').value;
    // console.log(firstname + lastname + email + password + password2);

    let formData = {
      firstname : firstname,
      lastname : lastname,
      email: email,
      password : password,
      password2 : password2
    }

    axios.post('http://localhost:5000/users/register', formData) //axios ginamit ko imbis na fetch pero same lng sila ng function 
    // sa AXIOS kasi kaunti lng ung matatype sa fetch need mo pa ideclare sa { } na second parameter nya dito binigay ko nlng agad ung data na pinasa 
    // pero same lng dn nmn
    //http://codeheaven.io/how-to-use-axios-as-your-http-client/
    .then(res => {
      // if may nakuha akong response na nireturn from back end eto mag eexecute
      document.querySelector('#firstErr').innerHTML = '';
      document.querySelector('#lastErr').innerHTML = '';
      document.querySelector('#emailErr').innerHTML = '';
      document.querySelector('#passwordErr').innerHTML = '';
      document.querySelector('#password2Err').innerHTML = '';
      document.querySelector('#firstErr').classList.remove('inputError');
      document.querySelector('#lastErr').classList.remove('inputError');
      document.querySelector('#emailErr').classList.remove('inputError');
      document.querySelector('#passwordErr').classList.remove('inputError');
      document.querySelector('#password2Err').classList.remove('inputError');
      $('#registerModal').modal('toggle');
      $('#loginModal').modal('toggle');
    })
    .catch(err => {
      // if nag bato ng error sa back end na pwedeng
      // res.status(400) <--- error to kaya si catch mkakatanggap !NOTE(base lang sakin to :D ) !NOTE2(nung tinry ko kasi sa fetch tong ganto)
      // si res(sa .then(res)) nkakatanggap nung error  
      if(err.response.data.firstname) { //check ko lng if ung (err.response.data.etc) may ganon ba tlga sa nireturn nating error from validation 
        // na ang nirereturn natin dito from back end ung (res.status(400).json(errors)) 
        document.querySelector('#firstname').classList.add('inputError');
        document.querySelector('#firstErr').innerHTML = err.response.data.firstname;
      } else {
        document.querySelector('#firstname').classList.remove('inputError');
        document.querySelector('#firstErr').innerHTML = '';
      }
      if(err.response.data.lastname) {
        document.querySelector('#lastErr').innerHTML = err.response.data.lastname;
        document.querySelector('#lastname').classList.add('inputError');
      }else {
        document.querySelector('#lastname').classList.remove('inputError');
        document.querySelector('#lastErr').innerHTML = '';
      }
      if(err.response.data.email) {
        document.querySelector('#email').classList.add('inputError');
        document.querySelector('#emailErr').innerHTML = err.response.data.email;
      }else {
        document.querySelector('#email').classList.remove('inputError');
        document.querySelector('#emailErr').innerHTML = '';
      }
      if(err.response.data.password) {
        document.querySelector('#password').classList.add('inputError');
        document.querySelector('#passwordErr').innerHTML = err.response.data.password;
      }else {
        document.querySelector('#password').classList.remove('inputError');
        document.querySelector('#passwordErr').innerHTML = '';
      }
      if(err.response.data.password2) {
        document.querySelector('#password2').classList.add('inputError');
        document.querySelector('#password2Err').innerHTML = err.response.data.password2;
      }else {
        document.querySelector('#password2').classList.remove('inputError');
        document.querySelector('#password2Err').innerHTML = '';
      }
    })
  }
  btnReg.addEventListener('click', registerClicked); //dito ko na tnawag ung const function 
}

let profileModal = document.querySelector('#profileModal'); //check ko lng if true na si profile Modal ay true
if(profileModal) { 
  function profileRender() { //nag create lng ako nang function para matawag ko ulit tong render method na to
    // pag nagupload para mag reflect agad if success ung upload .
    // eto innerHTML lng to tnawag ko lahat ng laman ng localStorage(data ng current user na nka log in) sa .profileModalBody which ung modal-body sa modal
    document.querySelector('.profileModalBody').innerHTML = `
    <div class="profileImg">
      <div class="profileHolder">
        <input type="file" hidden="hidden" name="image" class="hiddenFileUpload" data-id="${localStorage.getItem('id')}">
        <img src="http://localhost:5000/avatar/${localStorage.getItem('avatar')}" alt="">
        <div class="profileHoverUpload"><i class="fas fa-user-edit">Edit</i></div>
      </div>
    </div>
    <div class="nameHolder mt-3">
      <div class="form-group form-group-custom">
        <label for="firstname">Firstname</label>
        <input type="text" class="form-control input-reg" id="firstnameProfile" value="${localStorage.getItem('firstname')}">
        <span class="errors" id="firstProfileErr"></span>
      </div>
      <div class="form-group form-group-custom">
        <label for="lastname">Lastname</label>
        <input type="text" class="form-control input-reg" id="lastnameProfile" value="${localStorage.getItem('lastname')}">
        <span class="errors" id="lastProfileErr"></span>
      </div>
    </div>
    `
  }
  profileRender(); // eto ung first render if wala pang changes tnwag ko ung function profileRender para mag display don sa modal
  const triggerInput = function(e) { //event function na ttwagin sa addEventListener
    //NOTE event function since na ung profileHover ay late nang dinisplay may chance na d mkita ung class if direct natin tinawag through (querySelector)
    if($(e.target).hasClass('profileHoverUpload')) { //jquery check ko if ung(e.target) ay may class na hinahanap ko 
      $('.hiddenFileUpload').click(); // jquery same thing if naclick ko ung hnahanap kong trigger (which is ung hover sa ibabaw ng image) iclick nya lng ung hidden file input
    }
  }

  const tryUpload = function() { //eto ung magpapadala tayo ng request file pabackend(req.file.image don sa backend ang twag) 
    let image = document.querySelector('.hiddenFileUpload'); //eto ung hidden input 
    let id = document.querySelector('.hiddenFileUpload').dataset.id;
    let newFormImageData = new FormData(); //NOTE pag image ung inaano natin FormData gamit ko imbis na object
    newFormImageData.append('image', image.files[0]); //append ko lng image data (image.files ang laman neto array if ever na mag upload ka laging first ung file data [0]) 
    // axios.put lng para maipadala sa backend
    axios.put(`http://localhost:5000/users/profile/upload/${id}`, newFormImageData)
    .then(res => {
      console.log(res);
      // since na mag uupload tayo need natin tanggalin ung avatar(img_url) na nilagay nung unang log in natin and mag seset tayo ng bago
      localStorage.removeItem('avatar');
      localStorage.setItem('avatar', res.data.success);
      // after na magawa un tawagin natin ulit si profileRender na mag rerender ng new data sa MODAL PROFILE
      profileRender();
    })
    .catch(err => {console.log(err)})
  }

  const nameUpdate = function() {
    let id = document.querySelector('.hiddenFileUpload').dataset.id;
    let firstname = document.querySelector('#firstnameProfile').value;
    let lastname = document.querySelector('#lastnameProfile').value;

    let newNameFormData = { //object ang ipapadata
      firstname : firstname,
      lastname : lastname
    }
    // console.log(newNameFormData);
    axios.put(`http://localhost:5000/users/name/update/${id}`, newNameFormData) //pinasa ko ung object data para masend sa backend
    .then(res => {
      console.log(res);
      $('#profileModal').modal('toggle'); //if success at may response na natanggap toggle('open if d nkabukas', 'close if nkabkas') mo lng ung modal jquery ito 
      $('#messageModal').modal('toggle'); //message modal eto ung mag aoutput nung response na (res.json({success: "something"})) galing sa backend
        document.querySelector('.messageBody').innerHTML = res.data.success //pag dating dto ung nireturn na json nsa loob ng res.data + ung key(success) nung message
        const messageFunc = function(e) { //same thing ttwagin do sa click addEventListner
          if($(e.target).hasClass('messageOk')) {
            $('#messageModal').modal('toggle');
            localStorage.removeItem('fisrtname'); //remove item sa localStorage
            localStorage.removeItem('lastname'); //remove item sa localStorage
            localStorage.setItem('firstname', res.data.firstname); //set ung natanggap na bago
            localStorage.setItem('lastname', res.data.lastname); 
            window.location.replace('/');  //redirect lang pwede tong palitan ng location.reload() same thing lng kasi since SPA(single page app) naman
          } 
        }
        let message = document.querySelector('.messageFooter'); //selector kung saan nka laman ung mga buttons
        message.addEventListener('click', messageFunc); // click listener na tnawag dito ung messageFunc
        document.querySelector('.closeMessage').addEventListener('click', messageFunc); // same thing pero eto nmn ung X button don sa header
    })
    .catch(err => {
      // if na may error same thing lng sa taas sa validation add lng ugn inputError tpos labas sa span ung error message
      if(err.response.data.firstname) {
        document.querySelector('#firstnameProfile').classList.add('inputError');
        document.querySelector('#firstProfileErr').innerHTML = err.response.data.firstname;
      } else {
        document.querySelector('#firstnameProfile').classList.remove('inputError');
        document.querySelector('#firstProfileErr').innerHTML = '';
      }
      if(err.response.data.lastname) {
        document.querySelector('#lastProfileErr').innerHTML = err.response.data.lastname;
        document.querySelector('#lastnameProfile').classList.add('inputError');
      }else {
        document.querySelector('#lastnameProfile').classList.remove('inputError');
        document.querySelector('#lastProfileErr').innerHTML = '';
      }
    })
  }
  
  //eto na ung mga selector para don sa mga fucntion sa taas
  document.querySelector('.profileNameSubmit').addEventListener('click', nameUpdate);
  document.querySelector('.hiddenFileUpload').addEventListener('change', tryUpload); //listener para don sa image upload since hidden ung input listen lng sa changes if may nag upload
  document.querySelector('.profileModalBody').addEventListener('click', triggerInput);
}

if(btnLog) {
  const loginClicked = function() {
    let email = document.querySelector('#emailLog').value;
    let password =  document.querySelector('#passwordLog').value;
    let formLoginData = {
      email : email,
      password: password
    }
    axios.post('http://localhost:5000/users/login', formLoginData) //post method
      .then(res => {
        // console.log(res.data);
        document.querySelector('#emailLog').classList.remove('inputError');
        document.querySelector('#emailLogErr').innerHTML = '';
        document.querySelector('#passwordLog').classList.remove('inputError');
        document.querySelector('#passwordLogErr').innerHTML = '';
        // dito tayo nag seset ng localStorage pag log in
        localStorage.setItem('token', res.data.token)
        localStorage.setItem('email', res.data.user.email)
        localStorage.setItem('firstname', res.data.user.firstname)
        localStorage.setItem('lastname', res.data.user.lastname)
        localStorage.setItem('avatar', res.data.user.avatar)
        localStorage.setItem('id', res.data.user._id)
        localStorage.setItem('isAdmin', res.data.user.isAdmin)
        $('#loginModal').modal('toggle');
        location.reload();
      })
      .catch(err => {
        if(err.response.data.email) {
          document.querySelector('#emailLog').classList.add('inputError');
          document.querySelector('#emailLogErr').innerHTML = err.response.data.email;
        } else {
          document.querySelector('#emailLog').classList.remove('inputError');
          document.querySelector('#emailLogErr').innerHTML = '';
        }
        if(err.response.data.password) {
          document.querySelector('#passwordLog').classList.add('inputError');
          document.querySelector('#passwordLogErr').innerHTML = err.response.data.password;
        } else {
          document.querySelector('#passwordLog').classList.remove('inputError');
          document.querySelector('#passwordLogErr').innerHTML = '';
        }
      })
  }
  btnLog.addEventListener('click', loginClicked);
}


let logout = document.querySelector('.logout');
//  pag log out clear lng natin storage tpos sa backend .logout() function lng
if(logout) {
  console.log(logout);
  const logoutFunc = function () {
    fetch('http://localhost:5000/users/logout')
      .then((res)=> {
        return res.json();
      })
      .then((data) => {
        localStorage.clear();
        window.location.replace('/');
      })
      .catch(function(err) {
        console.log(err);
      })
    }
    logout.addEventListener('click', logoutFunc);
}

let classAdd = document.querySelector('.classAddSubmit');
if(classAdd){
  // addclass etong mga jquery nato para to sa datepicker at timepicker sa modal
  $("#date").datepicker({
    minDate:0
  });
  $('#fromTime').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '9',
    maxTime: '9:00pm',
    defaultTime: '9',
    startTime: '9:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });
  
  $('#toTime').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '9',
    maxTime: '9:00pm',
    defaultTime: '9',
    startTime: '9:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
  });

  const classAddSub = function() { //const function lng na ttwagin sa listener
    let classname = document.querySelector('#className').value;
    let description = document.querySelector('#description').value;
    let slots = document.querySelector('#slots').value;
    let date = document.querySelector('#date').value;
    let fromtime = document.querySelector('#fromTime').value;
    let totime = document.querySelector('#toTime').value;

    // console.log(`${classname} ${description} ${slots} ${date} ${fromtime} ${totime}`);
    let formclassData = {
      classname : classname,
      description: description,
      slots: slots,
      date: date,
      fromtime: fromtime,
      totime : totime
    }

    axios.post('http://localhost:5000/class/add', formclassData) //axios post 
    .then(res => {
      document.querySelector('#className').classList.remove('inputError');
      document.querySelector('#classNameErr').innerHTML = '';
      document.querySelector('#description').classList.remove('inputError');
      document.querySelector('#descriptionErr').innerHTML = '';
      document.querySelector('#slots').classList.remove('inputError');
      document.querySelector('#slotsErr').innerHTML = '';
      document.querySelector('#date').classList.remove('inputError');
      document.querySelector('#dateErr').innerHTML = '';
      document.querySelector('#fromTime').classList.remove('inputError');
      document.querySelector('#fromTimeErr').innerHTML = '';
      document.querySelector('#toTime').classList.remove('inputError');
      document.querySelector('#toTimeErr').innerHTML = '';
      $('#addClassesModal').modal('toggle');
      window.location.replace('/');
    })
    .catch(err => {
      console.log(err);
      if(err.response.data.classname) {
        document.querySelector('#className').classList.add('inputError');
        document.querySelector('#classNameErr').innerHTML = err.response.data.classname;
      } else {
        document.querySelector('#className').classList.remove('inputError');
        document.querySelector('#classNameErr').innerHTML = '';
      }
      if(err.response.data.description) {
        document.querySelector('#description').classList.add('inputError');
        document.querySelector('#descriptionErr').innerHTML = err.response.data.description;
      } else {
        document.querySelector('#description').classList.remove('inputError');
        document.querySelector('#descriptionErr').innerHTML = '';
      }
      if(err.response.data.slots) {
        document.querySelector('#slots').classList.add('inputError');
        document.querySelector('#slotsErr').innerHTML = err.response.data.slots;
      } else {
        document.querySelector('#slots').classList.remove('inputError');
        document.querySelector('#slotsErr').innerHTML = '';
      }
      if(err.response.data.date) {
        document.querySelector('#date').classList.add('inputError');
        document.querySelector('#dateErr').innerHTML = err.response.data.date;
      } else {
        document.querySelector('#date').classList.remove('inputError');
        document.querySelector('#dateErr').innerHTML = '';
      }
      if(err.response.data.fromtime) {
        document.querySelector('#fromTime').classList.add('inputError');
        document.querySelector('#fromTimeErr').innerHTML = err.response.data.fromtime;
      } else {
        document.querySelector('#fromTime').classList.remove('inputError');
        document.querySelector('#fromTimeErr').innerHTML = '';
      }
      if(err.response.data.totime) {
        document.querySelector('#toTime').classList.add('inputError');
        document.querySelector('#toTimeErr').innerHTML = err.response.data.totime;
      } else {
        document.querySelector('#toTime').classList.remove('inputError');
        document.querySelector('#toTimeErr').innerHTML = '';
      }
    })
  }

  classAdd.addEventListener('click', classAddSub)
}

let secClass = document.querySelector('.sec-class-show');



if(secClass) {
  axios.get('http://localhost:5000/class/all') //if na true ung secClass(class name sa front page) sa page 
  .then(res => {
    let indivClass = ``; //ttwagin to mmya para dito istore ung mga returned na divs pag nag map na
    res.data.map(classes => {
      //etong classesBtns function na to check lng if ung current user ay d pa enroll or enrolled na sa class and if ung class ay wala nang slots if admin nmn EDIT DELETE btn
      // ang irereturn
      // eto ang mag rereturn ng mga btns 
      function classesbtns() {
        if(localStorage.length == 0) { //if walang nka log in empty div lang instead na mag return ako ng button 
          return `<div> </div>`;
        }
        else { //may nka log in
          if(localStorage.getItem('isAdmin') == 'true') { //admin ba sya 
            // return nya lng edit and delete btns
            return `
            <button class="btn btn-classes-inter enroll-btn editBtn" data-id="${classes._id}" data-toggle="modal" data-target="#editClassModal">EDIT</button>
            <button class="btn btn-classes-inter enroll-btn deleteBtn" data-id="${classes._id}" data-toggle="modal" data-target="#deleteClassModal">DELETE</button>`
          } else {
            //else na d sya admin
            if(classes.enrolled.includes(localStorage.getItem('id'))) { //check mo if ung current id nung nkalogin ay nandon sa array ng mga enrolled if true
              // return mo enrolled btn
              return `<button class="btn btn-classes-inter enroll-btn enrollBtn" data-id="${classes._id}" data-toggle="modal" data-target="#enrollClassModal" disabled>ENROLLED</button>`
            } else { 
              //else d pa sya enrolled or 0 slots
              if (classes.slots !== 0) { 
                //if hindi true na 0 slots return enroll now
                return `<button class="btn btn-classes-inter enroll-btn enrollBtn" data-id="${classes._id}" data-toggle="modal" data-target="#enrollClassModal">ENROLL</button>`
              } else {
                //return 0 slot btn
                return `<button class="btn btn-classes-inter enroll-btn enrollBtn" data-id="${classes._id}" data-toggle="modal" data-target="#enrollClassModal" disabled>NO SLOTS</button>`
              }
            }
          }
        }
      }
      let dateFormat = moment(classes.date, "MM-DD-YYYY").format("MMM Do YY"); //gumamit ng MOMENT para iformat ung date na nkuha (instead na 06/30/19 mgiging jun 30th 19)
      // tnwag lng ung indivclass sa labas ng res.data.map para istore ung cards += para mag patong ung lahat ng namap cards
      // map function parang foreach lang dn
      indivClass += `
      <div class="class-card">
        <div class="front">
          <h1 class="front-title">${classes.classname}</h1>
          <p class="front-description">${classes.description}</p>
          <div class="front-datetime">
            <span class="front-datetime-date">${dateFormat}</span>
            <span class="front-datetime-time">${classes.fromtime} to ${classes.totime}</span>
          </div>
          <p class="front-slots">${classes.slots} slots left</p>
          <div class="front-bottom">
          `+ classesbtns() +`
          </div>
        </div>
        <div class="back">
          <span class="back-title">${classes.classname}</span>
        </div>
      </div>
      `
      // console.log(moment(res.data.date,'MM/DD/YYYY').format("MMM Do YY"));
    })
    secClass.innerHTML = indivClass; //secclass ung nsa welcome.blade to dito lahat ia output na patong patong na cards
  })
  .catch(err => console.log(err))


  const editTarget = function(e) {
    if($(e.target).hasClass('editBtn')) {
      let id = e.target.dataset.id;
      axios.get(`http://localhost:5000/class/edit/${id}`)
      .then(res => {
        let classEdit = res.data;
        let formEdit = `
          <input type="hidden" id="hiddenId" data-id = "${classEdit._id}" />
          <div class="form-group form-group-custom">
            <label for="className">Class Name</label>
            <input type="text" class="form-control input-reg" id="classNameEdit" value="${classEdit.classname}">
            <span class="errors" id="classNameErrEdit"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="description">Description</label>
            <input type="text" class="form-control input-reg" id="descriptionEdit" value="${classEdit.description}">
            <span class="errors" id="descriptionErrEdit"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="slots">Slots</label>
            <input type="number" class="form-control input-reg" id="slotsEdit" value="${classEdit.slots}">
            <span class="errors" id="slotsErrEdit"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="date">Date</label>
            <input type="text" class="form-control input-reg datePickerEdit" id="dateEdit" value="${classEdit.date}">
            <span class="errors" id="dateErrEdit"></span>
          </div>
          
          <div class="form-group form-group-custom">
            <label for="fromTime">From</label>
            <input type="text" class="form-control input-reg fromTimeEdit" id="fromTimeEdit" value="${classEdit.fromtime}">
            <span class="errors" id="fromTimeErrEdit"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="toTime">To</label>
            <input type="text" class="form-control input-reg toTimeEdit" id="toTimeEdit" value="${classEdit.totime}">
            <span class="errors" id="toTimeErrEdit"></span>
          </div>
        `

        document.querySelector('.editModalBody').innerHTML = formEdit;
        document.querySelector('.editFooterModal').innerHTML = `
          <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-submit updateClassBtn">Update</button>
        `
        
      })
      .catch(err => {console.log(err)})
    }
    
  }
  
  const deleteTarget = function(e) { //delete function lng to sa classes cards
      
    if($(e.target).hasClass('deleteBtn')) {
      let id = e.target.dataset.id;
      axios.get(`http://localhost:5000/class/deleteConfirm/${id}`) //d pamag dedelete may modal muna nag (are you sure you want to delete) kaya nag get tayo para mkuha ung info
      // ng current card na pinindot
      .then(res => { //since na isa lang huhugutin natin d na kailangan imap to res.data. + ung name na nung wanted data 
        document.querySelector('.deleteBodyModal').innerHTML = `<h3>Are you sure you want to delete ${res.data.classname} class?</h3>`
        document.querySelector('.deleteFooterModal').innerHTML = `
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-submit deleteConfirm" data-id="${res.data._id}">Delete</button>
        `
      })
      .catch(err => console.log(err))

    }
    
  } 
  let deleteConfirmation = document.querySelector('.deleteFooterModal'); // footer ng modal kung saan nkalagay ung mga btns na submit and cancel
  const delClassConfirm = function(e) {
    // console.log(e.target);
    if($(e.target).hasClass('deleteConfirm')) { //if ung target ung button na hinahanap natin para sa delete function
      let id = e.target.dataset.id; 
      axios.delete(`http://localhost:5000/class/delete/${id}`) //axios delete 
      .then(res => {
        window.location.replace('/'); //if success reload lng page
      })
      .catch(err => console.log(err))
    }
  }


  let editModal = document.querySelector('.editModalBody');
  const editFunctionality = function(e) {
    if($(e.target).hasClass('datePickerEdit')) {
      $(e.target).datepicker({
        minDate:0
      });
    }
    if($(e.target).hasClass('fromTimeEdit')) {
      $(e.target).timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '9',
        maxTime: '9:00pm',
        defaultTime: '9',
        startTime: '9:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
      });
    }
    if($(e.target).hasClass('toTimeEdit')) {
      
      $(e.target).timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        minTime: '9',
        maxTime: '9:00pm',
        defaultTime: '9',
        startTime: '9:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
      });
    }
  }

  let editSave = document.querySelector('.editFooterModal');
  const editSaveFunction = function(e) {
    if($(e.target).hasClass('updateClassBtn')){
      let id = document.querySelector('#hiddenId').dataset.id;
      let classname = document.querySelector('#classNameEdit').value;
      let description = document.querySelector('#descriptionEdit').value;
      let slots = document.querySelector('#slotsEdit').value;
      let date = document.querySelector('#dateEdit').value;
      let fromtime = document.querySelector('#fromTimeEdit').value;
      let totime = document.querySelector('#toTimeEdit').value;
      
      let formUpdateData = {
        classname : classname,
        description :description,
        slots : slots,
        date: date,
        fromtime: fromtime,
        totime: totime
      }
      axios.put(`http://localhost:5000/class/edit/save/${id}` , formUpdateData)
      .then(res => {
        console.log(res);
        $('#editClassModal').modal('toggle');
        $('#messageModal').modal('toggle');
        document.querySelector('.messageBody').innerHTML = res.data.success
        const messageFunc = function(e) {
          if($(e.target).hasClass('messageOk')) {
            $('#messageModal').modal('toggle');
            window.location.replace('/');  
          } 
        }
        let message = document.querySelector('.messageFooter');
        message.addEventListener('click', messageFunc);
        document.querySelector('.closeMessage').addEventListener('click', messageFunc);
      })
      .catch(err => {
        if(err.response.data.classname) {
          document.querySelector('#classNameEdit').classList.add('inputError');
          document.querySelector('#classNameErrEdit').innerHTML = err.response.data.classname;
        } else {
          document.querySelector('#classNameEdit').classList.remove('inputError');
          document.querySelector('#classNameErrEdit').innerHTML = '';
        }
        if(err.response.data.description) {
          document.querySelector('#descriptionEdit').classList.add('inputError');
          document.querySelector('#descriptionErrEdit').innerHTML = err.response.data.description;
        } else {
          document.querySelector('#descriptionEdit').classList.remove('inputError');
          document.querySelector('#descriptionErrEdit').innerHTML = '';
        }
        if(err.response.data.slots) {
          document.querySelector('#slotsEdit').classList.add('inputError');
          document.querySelector('#slotsErrEdit').innerHTML = err.response.data.slots;
        } else {
          document.querySelector('#slotsEdit').classList.remove('inputError');
          document.querySelector('#slotsErrEdit').innerHTML = '';
        }
        if(err.response.data.date) {
          document.querySelector('#dateEdit').classList.add('inputError');
          document.querySelector('#dateErrEdit').innerHTML = err.response.data.date;
        } else {
          document.querySelector('#dateEdit').classList.remove('inputError');
          document.querySelector('#dateErrEdit').innerHTML = '';
        }
        if(err.response.data.fromtime) {
          document.querySelector('#fromTimeEdit').classList.add('inputError');
          document.querySelector('#fromTimeErrEdit').innerHTML = err.response.data.fromtime;
        } else {
          document.querySelector('#fromTimeEdit').classList.remove('inputError');
          document.querySelector('#fromTimeErrEdit').innerHTML = '';
        }
        if(err.response.data.totime) {
          document.querySelector('#toTimeEdit').classList.add('inputError');
          document.querySelector('#toTimeErrEdit').innerHTML = err.response.data.totime;
        } else {
          document.querySelector('#toTimeEdit').classList.remove('inputError');
          document.querySelector('#toTimeErrEdit').innerHTML = '';
        }
      })
    }
  }
  
  
  const enrollGetData = function(e) {
    let id = e.target.dataset.id;
    if($(e.target).hasClass('enrollBtn')) {
      axios.get(`http://localhost:5000/class/enroll/${id}`)
      .then(res => {
        document.querySelector('.enrollBodyModal').innerHTML = `<h3>Are you sure you want to enroll in ${res.data.classname}</h3>`;
        document.querySelector('.enrollFooterModal').innerHTML = `
          <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-submit enrollCofirmBtn" data-id="${res.data._id}">Enroll</button>
        `
        
      })
      .catch(err => {console.log(err)});
    }
  }


  let enrollConfirmed = document.querySelector('.enrollFooterModal');
  const enrollProcess = function(e) {
    if($(e.target).hasClass('enrollCofirmBtn')) {
      let id = e.target.dataset.id;
      userid = localStorage.getItem('id');
      let newEnrollee = { 
        userid: userid
      }
      // console.log(id);
      axios.put(`http://localhost:5000/class/enrollConfirm/${id}`, newEnrollee)
      .then(res => {
        $('#enrollClassModal').modal('toggle');
        $('#messageModal').modal('toggle');
        document.querySelector('.messageBody').innerHTML = res.data.success
        const messageFunc = function(e) {
          if($(e.target).hasClass('messageOk')) {
            $('#messageModal').modal('toggle');
            window.location.replace('/');  
          }
        }
        let message = document.querySelector('.messageFooter');
        message.addEventListener('click', messageFunc);
        document.querySelector('.closeMessage').addEventListener('click', messageFunc);
      })
      .catch(err => {
        $('#enrollClassModal').modal('toggle');
        $('#messageErrorModal').modal('toggle');
        document.querySelector('.messageErrorBody').innerHTML = err.response.data.error
        const messageErrFunc = function(e) {
          if($(e.target).hasClass('messageErrOk')) {
            $('#messageErrorModal').modal('toggle');
           
          }
        }
        let message = document.querySelector('.messageErrorFooter');
        message.addEventListener('click', messageErrFunc);
        document.querySelector('.closeErrorMessage').addEventListener('click', messageErrFunc);
      })
    }
  }

  enrollConfirmed.addEventListener('click', enrollProcess);
  secClass.addEventListener('click', enrollGetData);
  deleteConfirmation.addEventListener('click', delClassConfirm);
  editModal.addEventListener('click', editFunctionality);
  editSave.addEventListener('click', editSaveFunction);
  secClass.addEventListener('click', editTarget);
  secClass.addEventListener('click', deleteTarget);
}

let owlOn = document.querySelector('.owl-carousel');
if(owlOn) {
  
  axios.get('http://localhost:5000/comment/all')
  .then(res=> {
    let stackComment = ``;
    
    res.data.map(indivComments => {
      function checkAdminDel() {
        if(localStorage.length == 0) {
           
           return `<button class="deleteTesti disableDeleteBtn btn" data-id="${indivComments._id}"><i class="fas fa-times"></i></button>`
        } else {
          if(localStorage.getItem('isAdmin') == 'true') {
           
            return ` <button class="deleteTesti btn" data-id="${indivComments._id}"><i class="fas fa-times"></i></button>`
          } else {
            return `<button class="deleteTesti disableDeleteBtn btn" data-id="${indivComments._id}"><i class="fas fa-times"></i></button>`
          }
        }
      }
      stackComment = `
        <div class="item">
          `+ checkAdminDel() +`
          <img src="http://localhost:5000/avatar/${indivComments.avatar}" alt="" />
          <div class="qouteCenter text-center"> <span class="openQ"></span> </div>
          <blockquote>
            
            <p>${indivComments.comment}</p>
            <span class="nameTesti">- ${indivComments.firstname} ${indivComments.lastname}</span>
            
          </blockquote>
        </div>
      `
      $('.owl-carousel')
      .trigger('add.owl.carousel', [$(stackComment), 0])
      .trigger('refresh.owl.carousel')

    })
    
    // document.querySelector('.owl-item').innerHTML = stackComment;
    // $('owl-carousel').trigger('initialize.owl.carousel');
  })
  .catch(err => console.log(err))
  
  if(localStorage.length == 0) {
    document.querySelector('.testimonial-container').style.display = 'none';
  } else {
    document.querySelector('.testimonial-container').style.display = 'block';
  }

  $('.owl-carousel').owlCarousel({
    center:true,
    loop:true,
    margin:10,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:true
        }
        // 1000:{
        //     items:3,
        //     nav:true
        // }
    }
  })

  

  const submitTestimonial = function() {
    let comment = document.querySelector('.testimonials-input').value;
    let userid = localStorage.getItem('id');
    let firstname = localStorage.getItem('firstname');
    let lastname = localStorage.getItem('lastname');
    let avatar = localStorage.getItem('avatar');
    
    let newTestimonial = {
      comment : comment,
      userid : userid,
      firstname : firstname,
      lastname : lastname,
      avatar : avatar
    }
    // console.log(newTestimonial);
    axios.post('http://localhost:5000/comment/add', newTestimonial)
    .then(res => {
      $('#messageModal').modal('toggle');
        document.querySelector('.messageBody').innerHTML = res.data.success
        const messageFunc = function(e) {
          if($(e.target).hasClass('messageOk')) {
            $('#messageModal').modal('toggle');
            window.location.replace('/');  
          }
        }
        let message = document.querySelector('.messageFooter');
        message.addEventListener('click', messageFunc);
        document.querySelector('.closeMessage').addEventListener('click', messageFunc);
    })
    .catch(err => {
      if(err.response.data.comment) {
        document.querySelector('#testimonialInput').classList.add('inputError');
        document.querySelector('#testiErr').innerHTML = err.response.data.comment;
      } else {
        document.querySelector('#testimonialInput').classList.remove('inputError');
        document.querySelector('#testiErr').innerHTML = '';
      }
    })
  }
  document.querySelector('.testimonialSubBtn').addEventListener('click', submitTestimonial);


  const deleteComment = function(e) {
    if($(e.target).hasClass('deleteTesti')) {
      let id = e.target.dataset.id;
      $('#deleteTestiQ').modal('toggle');
      document.querySelector('.deleteTestiBody').innerHTML = 'Are you sure you want to delete this testimonial';
      const deleteTestiConfirmed = function () {
        axios.delete(`http://localhost:5000/comment/delete/${id}`)
        .then(res => {
          // console.log(res);
          $('#deleteTestiQ').modal('toggle');
          $('#messageModal').modal('toggle');
          document.querySelector('.messageBody').innerHTML = res.data.success
          const messageFunc = function(e) {
            if($(e.target).hasClass('messageOk')) {
              $('#messageModal').modal('toggle');
              window.location.replace('/');  
            }
          }
          let message = document.querySelector('.messageFooter');
          message.addEventListener('click', messageFunc);
          document.querySelector('.closeMessage').addEventListener('click', messageFunc);
        })
        .catch(err => {console.log(err)})
      }
  
      document.querySelector('.deleteConfirmedBtn').addEventListener('click', deleteTestiConfirmed)
    }
    
  }
  document.querySelector('.carouselComment').addEventListener('click', deleteComment);
}


let adminGetUsers = document.querySelector('#allUsers');
if(adminGetUsers) {
    
  const getAllUsers = function() {
    axios.get('http://localhost:5000/users/all')
    .then(res => {
      let indivUser = ``;
      res.data.map(allUser => {
        function btnUpdateUser() {
          if(allUser.isAdmin !== true) {
            // return 'hindi admin to'
            return `<button class="btn makeAdmin " data-id="${allUser._id}" id="" data-toggle="modal">
              <i class="fas fa-plus"></i>
              <i class="fas fa-user-tie"></i>
            </button>`
          } else {
            // return 'admin to'
            return `<button class="btn removeAdmin " data-id="${allUser._id}" id="" data-toggle="modal">
              <i class="fas fa-minus"></i>
              <i class="fas fa-user-tie"></i>
            </button>`
          }
        } 
        indivUser += `
          <tr id="${allUser._id}">
            <td data-title="Name">${allUser.firstname} ${allUser.lastname}</td>
            <td data-title="Email">${allUser.email}</td>
            <td data-title="Admin">${allUser.isAdmin}</td>
            <td data-title="Action">`
            + btnUpdateUser() + `
              <button class="btn removeUser" data-id="${allUser._id}" id="" data-toggle="modal"><i class="fas fa-trash"></i></button>
            </td>
          </tr>

        `
      })
      document.querySelector('#tbodyUsers').innerHTML = indivUser;
    })
    .catch(err => {
      console.log(err);
    })
  }


  let modifyUser = document.querySelector('#tbodyUsers');
  
  const modifyUserFunc = function(e) {
    let id = e.target.dataset.id;
    if($(e.target).hasClass('makeAdmin')) {
      axios.put(`http://localhost:5000/users/admin/makeAdmin/${id}`)
      .then(res => {
        $('#messageModal').modal('toggle');
          document.querySelector('.messageBody').innerHTML = res.data.success
          const messageFunc = function(e) {
            if($(e.target).hasClass('messageOk')) {
              $('#messageModal').modal('toggle');
              getAllUsers();
            } 
          }
          let message = document.querySelector('.messageFooter');
          message.addEventListener('click', messageFunc);
          document.querySelector('.closeMessage').addEventListener('click', messageFunc);
      })
      .catch(err => {
        console.log(err);
      })
    }
    if($(e.target).hasClass('removeAdmin')) {
      axios.put(`http://localhost:5000/users/admin/removeAdmin/${id}`)
      .then(res => {
        $('#messageModal').modal('toggle');
          document.querySelector('.messageBody').innerHTML = res.data.success
          const messageFunc = function(e) {
            if($(e.target).hasClass('messageOk')) {
              $('#messageModal').modal('toggle');
              getAllUsers();
            } 
          }
          let message = document.querySelector('.messageFooter');
          message.addEventListener('click', messageFunc);
          document.querySelector('.closeMessage').addEventListener('click', messageFunc);
      })
      .catch(err => {
        console.log(err);
      })
    }
    if($(e.target).hasClass('removeUser')) {
      $('#deleteUserModal').modal('toggle');
      document.querySelector('.deleteUserBody').innerHTML = 'Are you sure you want to delete this user';
      const deleteUserFunc = function() {
        axios.delete(`http://localhost:5000/users/remove/${id}`)
        .then(res => {
          $('#deleteUserModal').modal('toggle');
          $('#messageModal').modal('toggle');
          document.querySelector('.messageBody').innerHTML = res.data.success
          const messageFunc = function(e) {
            if($(e.target).hasClass('messageOk')) {
              $('#messageModal').modal('toggle');
              getAllUsers();
            } 
          }
          let message = document.querySelector('.messageFooter');
          message.addEventListener('click', messageFunc);
          document.querySelector('.closeMessage').addEventListener('click', messageFunc);
        })
        .catch(err => {
          console.log(err);
        })
      }
      document.querySelector('.deleteUserConfirmedBtn').addEventListener('click', deleteUserFunc)
      
    }
  }
  
  modifyUser.addEventListener('click', modifyUserFunc);
  adminGetUsers.addEventListener('click', getAllUsers);
}

