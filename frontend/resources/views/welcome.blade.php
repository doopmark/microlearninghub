@extends('layouts.app')

@section('content')
    <section id="home" class="landing container-fluid">
      <div class="landing-divide">
      </div>
      <div class="landing-divide2">
      </div>
      <div class="landing-content">
        <h1 class="landing-content-title">LEARN MICROCONTROLLERS AND BUILD AWESOME PROJECTS!</h1>
        <button class="btn custom-btn">BOOK NOW</button>
      </div>
    </section>
    {{-- section 2--}}
    <section id="classes" class="sec-class container-fluid">
      <h1 class="sec-class-title text-center">Classes</h1> 
      <div class="container sec-class-show">
        
      </div>
    </section>
    {{-- section 3 --}}
    <section id="testimonial" class="testimonials">
      <h1 class="text-center testimonials-title">Testimonials</h1>
      <div class="wrapper px-0 container">
        <div class="owl-carousel owl-theme carouselComment">
        
        </div>  
        <div class="testimonial-container">
          <input name="" id="testimonialInput" class="testimonials-input" />
          <p class="errors text-center" id="testiErr"></p>
          <div class="text-center">
            <button class="btn testimonialSubBtn">Submit</button>
          </div>
        </div>
      </div>
      
    </section>
@endsection

@section('modals')
    <!--Register Modal -->
    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Register</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
              </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group form-group-custom">
                <label for="firstname">Firstname</label>
                <input type="text" class="form-control input-reg" id="firstname">
                <span class="errors" id="firstErr"></span>
              </div>
              <div class="form-group form-group-custom">
                <label for="lastname">Lastname</label>
                <input type="text" class="form-control input-reg" id="lastname">
                <span class="errors" id="lastErr"></span>
              </div>
              <div class="form-group form-group-custom">
                <label for="email">Email</label>
                <input type="email" class="form-control input-reg" id="email">
                <span class="errors" id="emailErr"></span>
              </div>
              <div class="form-group form-group-custom">
                <label for="password">Password</label>
                <input type="password" class="form-control input-reg" id="password">
                <span class="errors" id="passwordErr"></span>
              </div>
              <div class="form-group form-group-custom">
                <label for="password2">Confirm Password</label>
                <input type="password" class="form-control input-reg" id="password2">
                <span class="errors" id="password2Err"></span>
              </div>
              
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-submit btnRegSubmit">Submit</button>
          </div>
        </div>
      </div>
    </div>



<!-- Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group form-group-custom">
            <label for="email">Email</label>
            <input type="email" class="form-control input-reg" id="emailLog">
            <span class="errors" id="emailLogErr"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="password">Password</label>
            <input type="password" class="form-control input-reg" id="passwordLog">
            <span class="errors" id="passwordLogErr"></span>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-submit loginSubmit">Submit</button>
      </div>
    </div>
  </div>
</div>


<!--AddClasses Modal -->
<div class="modal fade" id="addClassesModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group form-group-custom">
            <label for="className">Class Name</label>
            <input type="text" class="form-control input-reg" id="className">
            <span class="errors" id="classNameErr"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="description">Description</label>
            <input type="text" class="form-control input-reg" id="description">
            <span class="errors" id="descriptionErr"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="slots">Slots</label>
            <input type="number" class="form-control input-reg" id="slots">
            <span class="errors" id="slotsErr"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="date">Date</label>
            <input type="text" class="form-control input-reg" id="date">
            <span class="errors" id="dateErr"></span>
          </div>
          
          <div class="form-group form-group-custom">
            <label for="fromTime">From</label>
            <input type="text" class="form-control input-reg" id="fromTime">
            <span class="errors" id="fromTimeErr"></span>
          </div>
          <div class="form-group form-group-custom">
            <label for="toTime">To</label>
            <input type="text" class="form-control input-reg" id="toTime">
            <span class="errors" id="toTimeErr"></span>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-submit classAddSubmit">Add</button>
      </div>
    </div>
  </div>
</div>


<!--edit Modal -->
<div class="modal fade" id="editClassModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body editModalBody">
        
      </div>
      <div class="modal-footer editFooterModal">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>


<!--Delete Modal -->
<div class="modal fade" id="deleteClassModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body deleteBodyModal ">
        
      </div>
      <div class="modal-footer deleteFooterModal">
       
      </div>
    </div>
  </div>
</div>


<!--Enroll Modal -->
<div class="modal fade" id="enrollClassModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Enroll Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body enrollBodyModal bodyMessageOnly">
        
      </div>
      <div class="modal-footer enrollFooterModal">
        
      </div>
    </div>
  </div>
</div>

<!--Message Modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Message</h5>
          <button type="button" class="close closeMessage" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body messageBody">
        Body
      </div>
      <div class="modal-footer messageFooter">
        <button type="button" class="btn btn-submit messageOk">OK</button>
      </div>
    </div>
  </div>
</div>
<!--Message Error Modal -->
<div class="modal fade" id="messageErrorModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Error</h5>
          <button type="button" class="close closeErrorMessage" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body messageErrorBody">
        Body
      </div>
      <div class="modal-footer messageErrorFooter">
        <button type="button" class="btn btn-submit messageErrOk">OK</button>
      </div>
    </div>
  </div>
</div>


<!--deleteTestimonial Modal -->

<div class="modal fade" id="deleteTestiQ" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body deleteTestiBody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-submit deleteConfirmedBtn">Yes</button>
      </div>
    </div>
  </div>
</div>


<!--profile Modal -->
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Profile</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body profileModalBody">
         
      </div>
      <div class="modal-footer profileModalFooter">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-submit profileNameSubmit">Save</button>
      </div>
    </div>
  </div>
</div>

<!--Users Modal -->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title ">USERS</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body">
        <div class="container">
        <div class="tablemc-responsive-vertical">
            <div class="container-fluid">
            </div>
            <table class="tablemc tablemc-bordered tablemc-striped tablemc-hover tablemc-mc-red">
                <thead>
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Admin</th>
                      <th>Action</th>
                    </tr>
                </thead>
                <tbody id="tbodyUsers">
                  {{-- <tr id="${user._id}">
                    <td data-title="Name">${user.firstname} ${user.lastname}</td>
                    <td data-title="Email">${user.email}</td>
                    <td data-title="Admin">${user.admin}</td>
                    <td data-title="Action">` 
                    + btnUpdateUser() + `
                      <button class="btn removeUser" data-id="${user._id}" id="" data-toggle="modal">REMOVE</button>
                    </td>
                  </tr> --}}
                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary">Save</button> --}}
      </div>
    </div>
  </div>
</div>

<!--global question Modal -->
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-times"></i></span>
          </button>
      </div>
      <div class="modal-body deleteUserBody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-submit deleteUserConfirmedBtn">Yes</button>
      </div>
    </div>
  </div>
</div>
@endsection