const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create UserSchema
const CommentSchema = new Schema({
  comment : {
    type : String,
    required : true
  },
  userid : {
    type : String,
    required : true
  },
  firstname : {
    type: String,
    required : true
  },
  lastname : {
    type: String,
    required : true
  },
  avatar : {
    type : String,
    required : true
  }
});

module.exports = Comment = mongoose.model('comments', CommentSchema);