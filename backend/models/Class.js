const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create UserSchema
const ClassSchema = new Schema({
  classname : {
    type : String,
    required : true
  },
  description : {
    type : String,
    required : true
  },
  slots : {
    type: Number,
    required : true
  },
  date : {
    type: String,
    required : true
  },
  fromtime : {
    type : String,
    required : true
  },
  totime : {
    type : String,
    required : true
  },
  enrolled : { //etong collection name enrolled ko ay array kasi mag nenest ako ng madami sknya mga students 
    type: Array,
    default: []
  }
});

module.exports = classes = mongoose.model('classes', ClassSchema); //module exports lang para magamit tong model schema