const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');

// routes
const users = require('./routes/API/users');
const classes = require('./routes/API/classes');
const comment = require('./routes/API/comments');


const app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());
const db = require('./config/keys').mongoURI;
mongoose.connect(db, {useNewUrlParser: true})
  .then(() => console.log('Connected to database'))
  .catch(err => console.log(err))


// make public to be accessible *http://localhost:5000/(public not rquired to insert)uploads/image.jpg
app.use(express.static('public/'));

app.use('/users', users);
app.use('/class', classes);
app.use('/comment', comment);
  
port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`server is running on ${port}`);
});