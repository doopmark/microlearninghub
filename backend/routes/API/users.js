const express = require('express'); //express is a framework for nodeJS 
const router = express.Router(); //calling the router function from express
const userModel = require('../../models/User');  //model for user 
const bcrypt = require('bcryptjs'); //this converts the string to hash
const jwt = require('jsonwebtoken'); //for creating a token to login
const keys = require('../../config/keys'); // mongoURL seperated js file (JSON)
// validation import
const validateRegisterInput = require('../../validation/register'); //validation for register input
const validateLoginInput = require('../../validation/login'); //validation for login input
const validateProfileInputs = require('../../validation/profile'); //validation for profile inputs
// multer (to upload images to backend)
const path = require('path'); //path is pre installed in node [use for extracting the extension file of the image (jpg,png,etc)]
const multer = require('multer'); //npm package use to upload images to folder

const passport = require('passport');

// storage to be called in multer function
const storage = multer.diskStorage({
  destination: 'public/avatar', 
  filename: function(req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
});
// upload is just a function name that calls multer
// this will be called later on when we get a request file image to upload
const upload = multer({
  storage : storage
}).single('image');

// ===========================REGISTER
// ===========================REGISTER
router.post('/register', (req,res) => { //router may mga function na POST/DELETE/GET/PUT 
  //etong variable const na to irereturn at pangcheck lang dito
  // katumbas ng {errors, isValid} na variable ay validateRegisterInput(req.body).errors & validateRegisterInput(req.body).isValid
  // mas pinadali lng natin ang pag tawag kasi eto ung nirereturn ng validation JS na nirequire natin
  const {errors, isValid} = validateRegisterInput(req.body);
  // check validation (isValid) that returned by validateRegisterInput
  if(!isValid) {
    return res.status(400).json(errors) // eto ung ipapasa na natin sa front end ung errors pag may d nasatisfied na condition
    //ang nirereturn nito ay (400 bad request) which is ang mkakatanggap nito sa front end ay si .catch 
    //at may ksmang json object na hawak lahat ng errors
  }
  
  userModel.findOne({ //tnawag ko ung userModel na nirequire sa taas at bawat model may mga function isa na tong findOne mag sesearch lng to sa mongoDB ng ISA
    email : req.body.email //eto ung ipapadala sa database na hahanapin nya [meaning nito hanapin mo sa mga EMAIL ng DB etong req.body.email(request na email galing sa front end) ]
  })
  .then(user => { //meaning kapag may nahanap ka ilagay mo sa USER  variable
    if(user) { //if true na may laman si USER 
      errors.email = 'Email already exist'; // tinawag ko ung errors na variable const sa taas nilagyan ko ng EMAIL error 
      return res.status(400).json(errors); //tpos irereturn ko ung lahat ng errors {}
    } else {
      const newUser = new userModel({ //pag walang nahanap mag cecreate ako ng bagong user kaya ininstantiate ko dito ung model
        firstname : req.body.firstname,
        lastname : req.body.lastname,
        email : req.body.email,
        password : req.body.password,
        isAdmin : req.body.isAdmin
      })
      bcrypt.genSalt(10, (err, salt) => { //after mkagawa need ko ihash password bcrpyt.gensalt anf function na gagamitin ung 10 kung ilang rounds ng salting sa password
        bcrypt.hash(newUser.password, salt, (err, hash) => {  //after masalt dito na ihahash ung pass tpos ung second parameter (hash) dito ipapasok ung hash na password
          newUser.password = hash; //tpos iOoverwrite dito ung password na gnawa natin knina sa taas para kay newUser
          newUser.save() //eto mag sesave papuntang db
            .then(user => res.json(user)) //pag naging problema rereturn ko tong user as response to frontend
            .catch(err => console.log(err)); //etong console log na to para magconsole lng sa terminal if magka problem
        })
      })
    }
  })
})

// ================================LOGIN
// ================================LOGIN

router.post('/login', (req, res) => { //POST
  const {errors, isValid} = validateLoginInput(req.body); 
  // check validation (isValid)
  if(!isValid) {
    return res.status(400).json(errors)
  }

  const email = req.body.email;
  const password = req.body.password;
  // Find user by email
  userModel.findOne({email})
    .then(user => {
      if(!user) {
        errors.email = "user not found";
        return res.status(404).json(errors)
      }
      // check password if skipped the if statement
      bcrypt.compare(password, user.password)
        .then(isMatch => {
          if(isMatch) {
            // User Matched
            const payload = {  //jwt payload
              _id : user._id,
              firstname : user.firstname,          
              lastname : user.lastname,
              email : user.email,
              isAdmin : user.isAdmin,
              avatar : user.avatar
            } 
            // sign/create a token
            jwt.sign(payload, keys.secetOrKey, {expiresIn : 3600}, (err, token) => {
              res.json({
                success: true,
                token : "Bearer " + token,
                user: payload
              })
            })
            
          } else {
            errors.password = 'Password incorrect';
            return res.status(400).json(errors);
          }
        })
    })
})

// =========================update profile pic
// =========================update profile pic

router.put('/profile/upload/:id', (req, res) => { //PUT function for update
  userModel.findOne({_id : req.params.id})
  .then(user => {
    upload(req, res, (err) => {   //after mkahanap ng user tinawag ko ung function na upload na const natin sa taas na may hawak hawak na multer fucntion 
      if(err){ // if mkatanggap ng error
        console.log(err); //console log lng
      } else {
        console.log(user.avatar); //if walang problem pwede kong gamitin ung USER var na pinagstoran ng nahanap na user
        user.avatar = req.file.filename //tinawag ko ung nahanap na user tpos ung avatar(img_url) USER.AVATAR tpos inoverwrite ko nung pinadalang image filename sa frontend (req.file.filename)
        user.save(); //save changes
      }
      return res.send({success : req.file.filename}); //etong return na to nirereturn ko lng ung uploaded filename
    })
  })
  .catch(err => { 
    return res.json({error : err});
  })

});

// ===========================LOGOUT
// ===========================LOGOUT
router.get('/logout', (req, res) => {
  // console.log(req);
  req.logout() //logoutOut function na hinugot sa passport na nirequire sa taas
  res.json({ //after maglogout send lng ng json {}
    status : 'logout',
    msg : 'you are logged out'
  })
})


router.put('/name/update/:id', (req, res) => {
  const {errors, isValid} = validateProfileInputs(req.body);
  // check validation (isValid)
  if(!isValid) {
    return res.status(400).json(errors)
  }
  
  userModel.findOne({_id : req.params.id})
  .then(userChange => {
    userChange.firstname = req.body.firstname;
    userChange.lastname = req.body.lastname;
    userChange.save();
    return res.json({
      success : 'Updated successfully',
      firstname : req.body.firstname,
      lastname : req.body.lastname
    })
  })
  .catch(err => {
    return res.json({error : err});
  })
})

router.get('/all', (req,res) => { //get function eto ung function na wala kang kahit anong request forms get lang nya lahat ng users
  userModel.find({}) // pag find lng ibigsabihin ifafind nya lahat ng mag match pero pag wala ka nmn binigay na option tulad nito kukunin nya lahat ng nsa loob ng db mo dpende
  // sa model (userModel for users)
  .then(allStudents => { // pag may nahanap sya sa db (na lahat nmn kinuha) store mo sa allStudends
    return res.json(allStudents); //return mo lahat ng students papuntang frontend
  })
  .catch(err => {
    return res.json({error : err});
  })
})

router.put('/admin/makeAdmin/:id', (req, res) => {
  userModel.findOne({_id : req.params.id})
  .then(makeAdmin => {
    makeAdmin.isAdmin = true;
    makeAdmin.save();
    return res.json({success : 'successfully make a new admin'}) 
  })
  .catch(err => {
    return res.json({error : err});
  })
})

router.put('/admin/removeAdmin/:id', (req, res) => {
  userModel.findOne({_id : req.params.id})
  .then(removeAdmin => {
    removeAdmin.isAdmin = false;
    removeAdmin.save();
    return res.json({success : 'User removed from admin'})
  })
  .catch(err => {
    return res.json({error : err});
  })
})

router.delete('/remove/:id' , (req, res) => {
  userModel.deleteOne({_id : req.params.id})
  .then(removeUser => {
    return res.json({success : 'User successfully deleted'})
  })
  .catch(err => {
    return res.json({error : err})
  })
})


module.exports = router;