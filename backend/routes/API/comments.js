const express = require('express');
const router = express.Router();
const commentModel = require('../../models/Comment');
const validateCommentInput = require('../../validation/comments');

router.get('/all', (req, res) => {
  commentModel.find({})
  .then(allComment => {
    return res.json(allComment)
  })
  .catch(err => res.json({error : err}))
})

router.post('/add' , (req, res) => {
  const {errors, isValid} = validateCommentInput(req.body);
  // check validation (isValid)
  if(!isValid) {
    return res.status(400).json(errors)
  }
  const newComment = new commentModel({
    comment : req.body.comment,
    userid : req.body.userid,
    firstname : req.body.firstname,
    lastname : req.body.lastname,
    avatar : req.body.avatar
  })
  newComment.save();
    return res.json({success : 'Testimonial successfully added'});
})


router.delete('/delete/:id', (req, res ) => {
  commentModel.deleteOne({_id : req.params.id})
  .then(success => {
    return res.json({success: "testiominal deleted"})
  })
  .catch(err => {
    return res.json({error : "bakit d ko mareturn ung res"})
  })
}) 

module.exports = router;