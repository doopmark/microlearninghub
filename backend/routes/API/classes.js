const express = require('express');
const router = express.Router();
const classModel = require('../../models/Class'); 
const keys = require('../../config/keys');

const userModel = require('../../models/User');
const validateClassesInput = require('../../validation/classes.js');


router.post('/add', (req,res) => {
  const {errors, isValid} = validateClassesInput(req.body);
  // check validation (isValid)
  if(!isValid) {
    return res.status(400).json(errors)
  }
  classModel.findOne({
    classname : req.body.classname
  })
  .then(classfound => {
    if(classfound) {
      errors.classname = 'this class is already added';
     return res.status(400).json(errors)
    }
    const newClass = new classModel({
      classname : req.body.classname,
      description: req.body.description,
      slots: req.body.slots,
      date: req.body.date,
      fromtime: req.body.fromtime,
      totime : req.body.totime,
      enrolled : []
    })
    newClass.save();
    return res.json({success : 'class successfully added'});
  })
  .catch(err => {
   return res.json({error : err})
  });
})

router.get('/all', (req, res) => {
  classModel.find({})
  .then(classes => {
    return res.json(classes)
  })
  .catch(err => {
    return res.json({error : err})
  })
})

router.get('/edit/:id', (req,res) => {
  classModel.findOne({_id : req.params.id})
  .then(classOne => {
    return res.json(classOne)
  })
  .catch(err => {
    return res.json({error : err})
  })
})

router.put('/edit/save/:id', (req, res) => {
  const {errors, isValid} = validateClassesInput(req.body);
  // check validation (isValid)
  if(!isValid) {
    return res.status(400).json(errors)
  }
  classModel.findOne({_id : req.params.id})
  .then(updateClass => {
    updateClass.classname = req.body.classname;
    updateClass.description = req.body.description;
    updateClass.slots = req.body.slots;
    updateClass.date = req.body.date;
    updateClass.fromtime = req.body.fromtime;
    updateClass.totime = req.body.totime;
    updateClass.save();
    return res.json({success : 'update successfull'})
  })
  .catch(err => {
    return res.json({error : err})
  })
})

router.get('/deleteConfirm/:id', (req,res) => {
  classModel.findOne({_id: req.params.id})
  .then(classConfirm => {
    return res.json(classConfirm)
  })
  .catch(err => {
    return res.json({error : err})
  })
})
router.delete('/delete/:id', (req,res) => {
  classModel.deleteOne({_id : req.params.id})
  .then(res => {
    return res.json({success : 'Successfully deleted class'})
  })
  .catch(err => {
    return res.json({error : err});
  })
})

router.get('/enroll/:id', (req,res) => {
  classModel.findOne({_id : req.params.id})
  .then(clickedClass => {
    return res.json(clickedClass);
  })
  .catch(err => {
    return res.json({error : err})
  })
})

router.put('/enrollConfirm/:id', (req,res) => {
  classModel.findOne({_id : req.params.id})
  .then(classToEnroll => {
    let studentsOfClass = classToEnroll.enrolled;
    if(classToEnroll.slots == 0) {
      return res.status(400).json({error : 'No slots available'});
    }
    if(studentsOfClass.includes(req.body.userid, -1)){
      return res.status(400).json({error : 'you are already enrolled to this class'});
    }
    classToEnroll.enrolled.push(req.body.userid);
    classToEnroll.slots -= 1;
    classToEnroll.save();
    return res.json({success : 'successfully enrolled to this class'})
  })
  .catch(err => {
    res.json({error : err})
  })
})


module.exports = router;