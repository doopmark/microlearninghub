const Validator = require('validator'); //etong validator ininstall to through npm install 
const isEmpty = require('./is-Empty.js'); //ni require dito ung isEmpty para magamit ung function nya

module.exports = function validateClassesInput(data) {
  let errors = {}; 
  // data.classname(etc) eto ung ipapasa nating data pag ginamit natin sa labas tong validator na to
  // basically chinecheck na dito kung may laman or wala
  data.classname = !isEmpty(data.classname) ? data.classname : ''; //eto ay ternary if statement lng to  
  data.description = !isEmpty(data.description) ? data.description : ''; // if TRUE ung (!isEmpty(data.something))
  data.slots = !isEmpty(data.slots) ? data.slots : ''; // which is may LAMAN return mo data.description else return ka ng empty string ('');
  data.date = !isEmpty(data.date) ? data.date : '';
  data.fromTime = !isEmpty(data.fromTime) ? data.fromTime : '';
  data.toTime = !isEmpty(data.toTime) ? data.toTime : '';

  //ginamit ko na dito ung validator sa baba para icheck pag may laman
  // (https://www.npmjs.com/package/validator)
  if(!Validator.isLength(data.classname, {min:4, max: 30})) { 
    errors.classname = 'Class Name must be at least 4 to 30 characters';
  }

  if(Validator.isEmpty(data.classname)) {
    errors.classname = ' class Name is required';
  }
  if(!Validator.isLength(data.description, {min:8, max: 200})) {
    errors.description = 'description must be at least 8 to 200 characters';
  }

  if(Validator.isEmpty(data.description)) {
    errors.description = ' class Name is required';
  }
  if(!Validator.isNumeric(data.slots)){
    errors.slots = 'Slots must be number';
  }
  if(Validator.isEmpty(data.slots)) {
    errors.slots = ' Slots is required';
  }
  if(Validator.isEmpty(data.date)) {
    errors.date = ' Date is required';
  }
 
  if(Validator.isEmpty(data.fromtime)) {
    errors.fromtime = ' Starting time is required';
  }
  if(Validator.isEmpty(data.totime)) {
    errors.totime = ' end time is required';
  }

  
  return {
    errors, //return ko lahat ng nakuha na errors sa taas magiging key value paired ang laman nito errors{classname : "ung error", etc : "etc"}
    isValid: isEmpty(errors) // sabi ko dati nagiging isValid : TRUE kapag [isEmpty(errors) which is errors : {} lang] if may laman ung errors nagiging FALSE lang to 
  }
  // eto na ung tntawag natin don sa labas na const {errors , isValid} = validationInput(req.body)
}