const Validator = require('validator');
const isEmpty = require('./is-Empty.js');

module.exports = function validateProfileInputs(data) {
  let errors = {};
  data.firstname = !isEmpty(data.firstname) ? data.firstname : '';
  data.lastname = !isEmpty(data.lastname) ? data.lastname : '';

  if(!Validator.isLength(data.firstname, {min: 2, max: 30})) {
    errors.firstname = 'firstname must be length of 2 to 30 characters';
  }

  if(!Validator.isLength(data.lastname, {min: 2, max: 30})) {
    errors.lastname = 'lastname must be length of 2 to 30 characters';
  }

  if(Validator.isEmpty(data.firstname)) {
    errors.firstname = 'Firstname is required';
  }

  if(Validator.isEmpty(data.lastname)) {
    errors.lastname = 'Lastname is required';
  }
  

  return {
    errors,
    isValid: isEmpty(errors)
  }
}