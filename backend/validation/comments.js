const Validator = require('validator');
const isEmpty = require('./is-Empty.js');

module.exports = function validateCommentInput(data) {
  let errors = {};
  data.comment = !isEmpty(data.comment) ? data.comment : '';

  if(!Validator.isLength(data.comment, {min:6, max: 90})) {
    errors.comment = 'Comment must be at least 4 to 90 characters';
  }

  if(Validator.isEmpty(data.comment)) {
    errors.comment = ' comment is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}