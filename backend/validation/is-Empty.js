const isEmpty = (value) => 
  value === undefined ||
  value === null ||
  (typeof value === 'object' && Object.keys(value).length === 0) ||
  (typeof value === 'string' && value.trim().length === 0)


module.exports = isEmpty; //inexport ko lng ung const isEmpty sa taas na function para magamit sa labas;

//etong isEmpty js na to pang validate lng mg ibat ibang uri ng empty returns na gagamitin sa mga validator js files natin na ksma nya sa folder